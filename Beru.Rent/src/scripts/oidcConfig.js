
//oidcConfig
export default {
  authority: 'https://localhost:7258',
  client_id: 'client_id_vue',
  redirect_uri: 'https://localhost:3000/callback',
  response_type: 'code',
  scope: 'openid profile Bff.Api'
};
